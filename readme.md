##Server Requirements
- PHP >= 5.5.9
- Composer
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

##Installation

- Clonar el repositorio
```bash 
    git clone {HTTP_URI_REPOSITORIO} {PATH_PROJECT}
```

- Acceder al proyecto y ejecutar los siquientes comandos
```bash 
    cd {PATH_PROJECT}
    chmod 777 -R storage bootstrap/cache
    cp .env.example env.
    composer install
```

- Configurar de la base de datos
```text
    DB_DATABASE=            DATABASE NAME
    DB_USERNAME=            DATABASE USERNAME
    DB_PASSWORD=            DATABASE PASSWORD
```

- Ejecutar los siquientes comandos para genera usuarios de pruebas
```bash 
    cd {PATH_PROJECT}
    php artisan migrate --seed 
    # User: default
    # Pass: secret // el password secret funciona para todos los usuarios
```

- Si no funciona el comando migrate
Puedes usar la copia del sql que esta en la carpeta storage/app/db.sql

- Tambien puede acceder al proyecto y interactuar con el a travez del siquiente link
```text
    http://e-framework-wmaster732.c9users.io/
```
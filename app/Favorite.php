<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'favorite_user_id',
    ];
    
    
    /**
     * Get the related users.
     */
    public function users()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * //
     */
    public function info()
    {
        return $this->hasOne('App\User', 'id', 'favorite_user_id');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Payment;
use Carbon\Carbon;

class PaymentController extends Controller
{
    public function index()
    {
        return view('app.payment.index', [
            'data' => Payment::paginate(5)
        ]);
    }
    
    public function create()
    {
        return view('app.payment.create');
    }
    
    
    public function store(Request $request)
    {
        $this->validate($request, [
            'dates' => 'required|date',
            'amount' => 'numeric',
        ]);
        
        if (!$payment = Payment::create([ 
            'amount' => $request->amount,
            'created_at' => Carbon::parse($request->dates)->toDateTimeString()
        ])) {
            notify()->flash('The payment could not be generated', 'warning');
        }
        
        foreach(User::all() as $user){
            $payment->users()->attach( $user->id);
        }
        notify()->flash('The payment was correctly generated', 'success');
        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Favorite;

class UserController extends Controller
{

    public function index()
    {
        return view('app.user.index', [
            'favorites' => Auth::user()->favorites,
            'users' => User::paginate(5)
        ]);
    }

    /*
    * Renderiza la vista con los datos del usuarios.
    */
    public function profile()
    {
        return view('app.user.profile', [
            'data' => Auth::user()
        ]);
    }
    
    /*
    * Renderiza la vista con usuario.
    */
    public function show($id)
    {
       return view('app.user.show', [
            'data' => User::find($id)
        ]);
    }
    
    /*
    * View de crear usuarios.
    */
    public function create()
    {
        return view('app.user.create');
    }
    
    /*
    * crear usuarios.
    */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|alpha_num|unique:users',
            'age' => 'numeric|min:18',
            'password' => 'required|confirmed|min:6'
        ]);
        
        $data = $request->only(['username', 'age']);
        $data['password'] = bcrypt($request->password);
        
        if (!User::create($data)) {
            notify()->flash('The user could not be created', 'warning');
        }
        notify()->flash('User successfully created', 'success');
        return redirect()->back();
    }

    /*
    * Cambia la contraseña del usuario.
    */
    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);

        if (Auth::user()->update([
            'password' => bcrypt($request->password)
        ])) {
            return redirect()->back();
        }
        return redirect()->back()
                    ->withErrors(['password' => trans('password.reset')]);
    }

    /*
    * Renderiza la vista de usuarios favoritos.
    */
    public function favorites()
    {
        $user = User::with('favorites.info')->find(Auth::user()->id);
        return view('app.user.favorites', [
            'favorites' => $user->favorites
        ]);
    }
    
    /*
    * Crear Usuario favorito
    */
    public function favoriteCreate(Request $request)
    {
        if(!Auth::user()->favorites()->create(['favorite_user_id' => $request->f_id])) {
            notify()->flash('The user could not be added to favorites', 'warning');
        }
        notify()->flash('The user was added to favorites', 'success');
        return redirect()->back();
    }

    /*
    * Eliminar Usuario favorito
    */
    public function favoriteDelete(Request $request)
    {
        if (!Favorite::where('user_id', Auth::id())->where('favorite_user_id', $request->f_id)->delete()) {
            notify()->flash('The user could not be removed from your favorites', 'warning');
        }
        notify()->flash('The user is no longer in your favorites', 'success');
        return redirect()->back();
    }
    
    public function payments()
    {
        return view('app.user.payment', [
            'payments' => Auth::user()->payments
        ]);
    }
}

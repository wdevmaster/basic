<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'age',
        'username',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];
    
    /**
     * Get the payments.
     */
    public function payments()
    {
        return $this->belongsToMany('App\Payment');
    }

    /**
     * Get the favorite users.
     */
    public function favorites()
    {
        return $this->hasMany('App\Favorite');
    }

    /**
     * //
     */
    public function like()
    {
        return $this->belongsTo('App\Favorite', 'favorite_user_id', 'id');
    }
}

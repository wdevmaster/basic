<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'username' => 'default'
        ]);
        
        
        for($i = 0; $i < 50; $i++)
            factory(\App\User::class)->create();
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Payments
                    <a href="{{ route('payment.create') }}" class="pull-right btn btn default">
                        Generate payment
                    </a>
                </div>
        
                <div class="panel-body">
                    @if($data->count() == 0)
                    <div class="alert alert-danger">
                        There are no registered payments
                    </div>
                    @endif
                    <div class="list-group">
                        @foreach($data as $payment)
                        <li class="list-group-item">
                            <h4 class="list-group-item-heading">
                                <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span>
                                Payment {{ $payment->id }}
                            </h4>
                            <p class="list-group-item-text text-right">
                                <h4 class="text-right">${{ $payment->amount }}</h4>
                            </p>
                            <p class="list-group-item-text text-right">
                                {{ $payment->created_at }} <!-- toFormattedDateString(); diffInMinutes();  -->
                            </p>
                        </li>
                        @endforeach
                    </div>
                    <div class="pull-right">
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

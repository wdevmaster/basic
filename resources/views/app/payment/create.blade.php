@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Generate payment
                    <a href="{{ route('payments.index') }}" class="pull-right btn btn default">Go back</a>
                </div>
        
                <div class="panel-body">
        
                    <form class="form-horizontal" method="POST" action="{{ route('payment.store') }}">
                                {{ csrf_field() }}
        
                        <div class="form-group{{ $errors->has('dates') ? ' has-error' : '' }}">
                            <label for="dates" class="col-md-4 control-label">Date</label>
        
                            <div class="col-md-6">
                                <input id="dates" type="date" class="form-control" name="dates" value="{{ old('dates') }}" required autofocus>
        
                                @if ($errors->has('dates'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('dates') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                
                        <div class="form-group{{ $errors->has('amount') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Amount</label>
            
                            <div class="col-md-6">
                                <input id="amount" type="text" class="form-control" name="amount" value="{{ old('amount') }}" required autofocus>
            
                                @if ($errors->has('amount'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('amount') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
        
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-success">
                                    Save
                                </button>
                                <a href="#" clas="btn btn-default">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

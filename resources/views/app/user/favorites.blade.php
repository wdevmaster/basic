@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">User Favorites</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="list-group">
                        @foreach($favorites as $f)
                        <li class="list-group-item">
                            <h4 class="list-group-item-heading">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <a href="{{ route('user.show', $f->info->id) }}">{{ $f->info->username }}</a> 
                            </h4>
                            <p class="list-group-item-text">
                                created: {{$f->info->created_at ?: "0000-00-00 00:00:00"}}
                            </p>
                            <button 
                               class="btn-favorite btn btn-default"
                               data-id="{{$f->favorite_user_id}}" 
                               style="float: right; margin-top: -39px; font-sizes: 14px; color: #ff0000">
                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                            </button>   
                        </li>
                        @endforeach
                        
                        @if($favorites->count() == 0)
                            <div class="alert alert-danger">
                                No favorite users
                            </div>
                        @endif
                    </div>
                    <form id="favorite-delete-form" action="{{ route('user.favorite.delete') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input id="favorite-id" type="hidde" name="f_id" value="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $('.btn-favorite').on('click', function () {
        $('#favorite-id').val($(this).data('id'))
        document.getElementById('favorite-delete-form').submit()
    })

</script>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    User
                    <a href="{{ route('users.index') }}" class="pull-right btn btn default">Go back</a>
                </div>
        
                <div class="panel-body">
                    <div class="col-md-4">
                        <label for="email" class="col-md-5 control-label">Username</label>
                        {{$data->username}}
                    </div>
                    
                    <div class="col-md-4">
                        <label for="email" class="col-md-5 control-label">Age</label>
                        {{$data->age}}
                    </div>
                    
                    <div class="col-md-4">
                        <label for="email" class="col-md-5 control-label">Created</label>
                        {{$data->created_at}}
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
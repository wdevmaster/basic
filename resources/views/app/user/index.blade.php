@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Users
                    <a href="{{ route('user.create') }}" class="pull-right btn btn default">Create</a>
                </div>
        
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
        
                    <div class="list-group">
                        @foreach($users as $user)
                        <li class="list-group-item">
                            <h4 class="list-group-item-heading">
                                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                <a href="{{ route('user.show', $user->id) }}">
                                    {{ $user->id == Auth::id() ? "You" : $user->username }}
                                </a> 
                            </h4>
                            <p class="list-group-item-text">
                                created: {{$user->created_at ?: "0000-00-00 00:00:00"}}
                            </p>
                            @if ($user->id != Auth::id())
                            <button 
                               class="btn-favorite btn btn-default"
                               data-id="{{ $user->id }}"
                               data-type="{{ $favorites->where('favorite_user_id', $user->id)->count() }}"
                               style="float: right; 
                                        margin-top: -39px; 
                                        font-sizes: 14px; 
                                        @if ($favorites->where('favorite_user_id', $user->id)->count())
                                        color: #ff0000
                                        @else
                                        color: #dfe3ee
                                        @endif
                                     ">
                                <span class="glyphicon glyphicon-heart" aria-hidden="true"></span>
                            </button>
                            @endif
                                    
                        </li>
                        @endforeach
                    </div>
                    <div class="pull-right">
                        {{ $users->links() }}
                    </div>
                    <form id="favorite-form" action="" method="POST" style="display: none;">
                        {{ csrf_field() }}
                        <input id="favorite-id" type="hidde" name="f_id" value="">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    
    var delet = "{{ route('user.favorite.delete') }}"
    var creat = "{{ route('user.favorite.create') }}"

    $('.btn-favorite').on('click', function () {
        $('#favorite-id').val($(this).data('id'))
        console.log($(this).data('id'))
        if ($(this).data('type')) $('#favorite-form').attr('action', delet)
        else $('#favorite-form').attr('action', creat)

        document.getElementById('favorite-form').submit()
    })

</script>
@endsection


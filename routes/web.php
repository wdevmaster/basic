<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Route::get('login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('login', 'Auth\LoginController@login');

Route::post('logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

Route::group(['middleware' => 'auth'], function () {
    Route::get('dashboard', [
        'as' =>'home',
        'uses' => 'HomeController@index'
    ]);

    Route::get('users', [
        'as' => 'users.index',
        'uses' => 'UserController@index'
    ]);

    Route::group(['prefix' => 'user'], function () {
        Route::get('profile', [
            'as' => 'user.profile',
            'uses' => 'UserController@profile'
        ]);
        
        Route::get('{id}/show', [
            'as' => 'user.show',
            'uses' => 'UserController@show'
        ]);
        
        Route::get('create', [
            'as' => 'user.create',
            'uses' => 'UserController@create'
        ]);
        
        Route::post('store', [
            'as' => 'user.store',
            'uses' => 'UserController@store'
        ]);

        Route::post('change/password', [
            'as' => 'user.change.password',
            'uses' => 'UserController@changePassword'
        ]);

        Route::post('delete', [
            'as' => 'user.delete',
            'uses' => 'UserController@delete'
        ]);
        
        Route::get('payments', [
            'as' => 'user.payments',
            'uses' => 'UserController@payments'
        ]);

        Route::get('favorites', [
            'as' => 'user.favorites',
            'uses' => 'UserController@favorites'
        ]);
        
        Route::post('favorite/create', [
            'as' => 'user.favorite.create',
            'uses' => 'UserController@favoriteCreate'
        ]);

        Route::post('favorite/delete', [
            'as' => 'user.favorite.delete',
            'uses' => 'UserController@favoriteDelete'
        ]);
    });
    
    Route::get('paymants', [
        'as' => 'payments.index',
        'uses' => 'PaymentController@index'
    ]);
    
    Route::group(['prefix' => 'payment'], function () {
        Route::get('create', [
            'as' => 'payment.create',
            'uses' => 'PaymentController@create'
        ]);
        
        Route::post('store', [
            'as' => 'payment.store',
            'uses' => 'PaymentController@store'
        ]);
    });

});
